package pc.modelling

import scala.util.Random

trait CoreCTMC[A] {
  def nextWithRate(a: A): Set[(Double,A)]
}

trait CTMC[A] extends CoreCTMC[A] with System[A] {

  def next(a: A): Set[A] = nextWithRate(a) map (_._2)

  def nextWithRate(a: A): Set[(Double,A)]

  def simulation(a0: A, rnd: Random): Stream[(Double,A)] =
    Stream.iterate( (0.0,a0) ){ case (t,a) => {
      val next = nextWithRate(a).toList
      if (next.isEmpty) (t,a) else {
        val rates = next.map(_._1).sum
        val rnd1 = rnd.nextDouble()
        val cumulative = next.scanLeft((0.0, a0)) { case ((r,a),(r2,a2)) => (r+r2/rates,a2) }.tail
        val choice = cumulative.collect { case (p, a) if p >= rnd1 => a }.head
        //println(a,next,rates,cumulative,rnd1,choice)
        (t + Math.log(1 / rnd.nextDouble()) / rates, choice)
      }
    }}
}

object CTMC {

  def ofFunction[A](f: PartialFunction[A,Set[(Double,A)]]): CTMC[A] = new CoreCTMC[A] with CTMC[A]{
    override def nextWithRate(a: A) = f.applyOrElse(a,(x: A)=>Set[(Double,A)]())
  }

  def ofRelation[A](r: Set[(A,Double,A)]): CTMC[A] = ofFunction{ case a =>
    r filter {_._1 == a} map {t=>(t._2,t._3)}
  }

  def ofTransitions[A](r: (A,Double,A)*): CTMC[A] = ofRelation(r.toSet)
}

object TryCTMC extends App {
  object State extends Enumeration {
    val idle,send,done,fail = Value
  }
  import State._

  val channel: CTMC[State.Value] = CTMC.ofTransitions(
    (idle,1.0,send),
    (send,100000.0,send),
    (send,200000.0,done),
    (send,100000.0,fail),
    (fail,100000.0,idle),
    (done,1.0,done)
  )
  println(channel.simulation(idle, new Random()).take(10).toList.mkString("\n"))
}

object TheSimulator2 extends App {
  object State extends Enumeration {
    val idle,send,done,fail = Value
  }
  import State._

  val channel: CTMC[State.Value] = CTMC.ofTransitions(
    (idle,1.0,send),
    (send,100000.0,send),
    (send,200000.0,done),
    (send,300000.0,fail),
    (fail,100000,idle),
    (done,1.0,done)
  )

  private final implicit class SimulationBatch[A](runs: Seq[List[(Double, A)]]) {
    def countReachingState(state: State.Value): Int =
      runs.count(r => r.exists(_._2.equals(state)))
  }

  def nRuns[A](n: Int, samples: Int,  channel: CTMC[A], a0: A): Seq[List[(Double, A)]] =
    for (_ <- 0 until n) yield channel.simulation(a0, new Random()).take(samples).toList

  def avgTimeUntilDone[A](runs: Seq[List[(Double, A)]]): Double =
    runs.flatMap(r => r.collectFirst { case x if x._2.equals(done) => x._1 })
      .sum / runs.countReachingState(done)

  def avgTimeFailState[A](runs: Seq[List[(Double, A)]]): Double =
    runs.map(r => r.sliding(2)
      .filter(x => x.head._2.equals(fail))
      .map(x => x.last._1 - x.head._1).sum / avgTimeUntilDone(Seq(r)))
      .sum / runs.countReachingState(fail)

  def avgTimeFailState2[A](runs: Seq[List[(Double, A)]]): Double =
    runs.map(r => r.sliding(2).foldLeft(0.0){
      case (acc, t) if t.head._2.equals(fail) => acc + (t.last._1 - t.head._1)
      case (acc, _) => acc
    } / avgTimeUntilDone(Seq(r))).sum / runs.countReachingState(fail)

  val a = List(
    (0.0, idle), (0.25, send),
    (0.3, fail), (0.6, idle), (0.85, send), // 0.3
    (0.9, fail), (1.0, idle), // 0.1
    (1.1, send), (1.2, done)) // done: 1.2

  val b = List(
    (0.0, idle), (0.25, send),
    (0.3, fail), (0.5, idle), //0.2
    (0.75, send), (0.85, done)) // done: 0.80

  val runs = Seq(a, b) //nRuns(10, 20, channel, idle)
  println(runs.map(r => r.mkString("\n")))
  println(runs.countReachingState(done), runs.countReachingState(fail))
  println(avgTimeUntilDone[State.Value](runs))
  println(avgTimeFailState[State.Value](runs))
  println(avgTimeFailState2[State.Value](runs))
}