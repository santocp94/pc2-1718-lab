package pc.modelling

trait CoreSystem[A] {

  def next(a: A): Set[A]
}

trait System[A] extends CoreSystem[A] {

  def normalForm(a: A): Boolean =
    next(a).isEmpty

  def complete(p: List[A]): Boolean =
    normalForm(p.last)

  def paths(a: A, depth: Int): Stream[List[A]] = {
    if (depth == 0)
      Stream()
    else if (depth == 1 || normalForm(a))
      Stream(List(a))
    else
      for (path <- paths(a, depth - 1);
           next <- next(path.last)) yield (path :+ next)
  }

  // might loop, use with care!
  def completePaths(a: A): Stream[List[A]] =
    Stream.iterate(1)(_+1) flatMap (paths(a,_)) filter (complete(_))
}

object System { // Our factory of Systems

  // The most general case, even an intensional one
  def ofFunction[A](f: PartialFunction[A,Set[A]]): System[A] = new CoreSystem[A] with System[A] {
    override def next(a: A) = f.applyOrElse(a,(x: A)=>Set[A]())
  }

  // Extensional specification
  def ofRelation[A](r: Set[(A,A)]): System[A] = ofFunction{
    case a => r filter (_._1 == a) map (_._2)
  }

  // Extensional with varargs.. note binary tuples can be defined by a->b
  def ofTransitions[A](r: (A,A)*): System[A] = ofRelation(r.toSet)
}

object TrySystem extends App {
  // Specification of my data-type for states
  object state extends Enumeration {
    val idle,send,done,fail = Value
  }
  type State = state.Value
  import state._

  // System specification
  val channel: System[State] = System.ofTransitions(
    idle->send,send->send,send->done,send->fail,fail->idle //,done->done
  )

  // Analysis, by querying
  println(channel.normalForm(idle))
  println(channel.normalForm(done))
  println(channel.next(idle))
  println(channel.next(send))
  println("P1  "+channel.paths(idle,1).toList)
  println("P2  "+channel.paths(idle,2).toList)
  println("P3  "+channel.paths(idle,3).toList)
  println("P4  "+channel.paths(idle,4).toList)
  println("CMP:\n"+channel.completePaths(idle).take(10).toList.mkString("\n"))
}